package com.natsu.mrhead

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.CheckBox
import android.widget.ImageView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //Image
        val mrBody = findViewById<ImageView>(R.id.mrbody)
        val mrHair = findViewById<ImageView>(R.id.mrhair)
        val mrEyeB = findViewById<ImageView>(R.id.mralis)
        val mrEye = findViewById<ImageView>(R.id.mreye)
        val mrBeard = findViewById<ImageView>(R.id.mrmouth)
        val mrMous = findViewById<ImageView>(R.id.mrkumis)
        //Checkbox
        val checkHair:CheckBox = findViewById(R.id.checkRambut)
        val checkEyeB:CheckBox = findViewById(R.id.checkAlis)
        val checkMous:CheckBox = findViewById(R.id.checkKumis)
        val checkBeard:CheckBox = findViewById(R.id.checkJanggut)
        checkHair.setOnCheckedChangeListener { buttonView, isChecked ->
            if (!isChecked) {
                mrHair.visibility = View.INVISIBLE
            }
            else {
                mrHair.visibility = View.VISIBLE
            }
        }
        checkEyeB.setOnCheckedChangeListener { buttonView, isChecked ->
            if (!isChecked) {
                mrEyeB.visibility = View.INVISIBLE
            }
            else {
                mrEyeB.visibility = View.VISIBLE
            }
        }
        checkMous.setOnCheckedChangeListener { buttonView, isChecked ->
            if (!isChecked) {
                mrMous.visibility = View.INVISIBLE
            }
            else {
                mrMous.visibility = View.VISIBLE
            }
        }
        checkBeard.setOnCheckedChangeListener { buttonView, isChecked ->
            if (!isChecked) {
                mrBeard.visibility = View.INVISIBLE
            }
            else {
                mrBeard.visibility = View.VISIBLE
            }
        }
    }
}